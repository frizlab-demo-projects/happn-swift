// swift-tools-version:5.2
import PackageDescription


let package = Package(
	name: "happn-swift",
	platforms: [
		.macOS(.v10_15)
	],
	dependencies: [
		.package(url: "https://github.com/vapor/vapor.git", from: "4.0.0"),
		.package(url: "https://github.com/vapor/jwt-kit.git", from: "4.0.0-rc"),
		.package(url: "https://github.com/vapor/fluent.git", from: "4.0.0-rc"),
		.package(url: "https://github.com/vapor/fluent-sqlite-driver.git", from: "4.0.0-rc")
	],
	targets: [
		.target(
			name: "HappnSwiftApp",
			dependencies: [
				.product(name: "Vapor", package: "vapor"),
				.product(name: "JWTKit", package: "jwt-kit"),
				.product(name: "Fluent", package: "fluent"),
				.product(name: "FluentSQLiteDriver", package: "fluent-sqlite-driver")
			]
		),
		.target(
			name: "happn-swift",
			dependencies: [
				"HappnSwiftApp",
				.product(name: "Vapor", package: "vapor")
			]
		),
		.testTarget(
			name: "HappnSwiftAppTests",
			dependencies: [
				"HappnSwiftApp",
				.product(name: "XCTVapor", package: "vapor")
			]
		),
	]
)
