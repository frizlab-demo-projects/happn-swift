import Foundation
import JWTKit
import Vapor



struct AuthToken : JWTPayload {
	
	/** The audience of the token. It’s “happn”; see JWT specs for more info */
	var aud = "happn"
	
	/** A unique identifier for the token (for instance to be able to revoke it) */
	var jti = UUID().uuidString
	
	/** The id of the user represented by the token */
	var sub: String
	
	/** The expiration date of the token */
	var exp: Date
	
	func verify(using signer: JWTSigner) throws {
		guard aud == "happn" else {throw Abort(.unauthorized)}
		guard Date() < exp else {throw Abort(.unauthorized, reason: "Token expired")}
	}
	
}
