import Vapor
import JWTKit



struct UserBearerAuthenticator : BearerAuthenticator {
	
	func authenticate(bearer: BearerAuthorization, for request: Request) -> EventLoopFuture<Void> {
		return request.eventLoop.future()
		.flatMapThrowing{ _ in
			let token: AuthToken = try JWTSigner.hs256(key: request.application.jwtSecret).verify(bearer.token)
			return token.sub
		}
		.flatMap{ userId in
			return User.find(userId, on: request.db)
		}
		.unwrap(or: Abort(.unauthorized, reason: "The user in the token was not found."))
		.map{ user in
			request.auth.login(user)
			return ()
		}
	}
	
}
