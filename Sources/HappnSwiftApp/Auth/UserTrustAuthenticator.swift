import Vapor



struct UserTrustAuthenticator : CredentialsAuthenticator {
	
	typealias Credentials = ApiLoginPostData
	
	func authenticate(credentials: ApiLoginPostData, for request: Request) -> EventLoopFuture<Void> {
		return request.eventLoop.future()
		.flatMapThrowing{ _ in
			/* Let’s make sure the user has a password */
			guard credentials.iHaveThePassword else {
				throw Abort(.unauthorized, reason: "You must have the password to login")
			}
		}
		.flatMap{ _ in
			return User.find(credentials.userId, on: request.db)
		}
		.unwrap(or: Abort(.unauthorized, reason: "The user was not found."))
		.map{ user in
			request.auth.login(user)
			return ()
		}
	}
	
}
