import Fluent
import Foundation
import Vapor



final class LikesController {
	
	func get(req: Request) throws -> EventLoopFuture<ApiLike> {
		guard let userIDFrom = req.parameters.get("userIDFrom"), let userIDTo = req.parameters.get("userIDTo") else {
			throw Abort(.internalServerError, reason: "userIDFrom or userIDTo is not set in the request")
		}
		return UserUserLike
		.query(on: req.db)
		.filter(\.$from.$id == userIDFrom)
		.filter(\.$to.$id == userIDTo)
		.all()
		.flatMapThrowing{ likes in
			guard let like = likes.first else {throw Abort(.notFound)}
			guard likes.count == 1 else {throw Abort(.internalServerError, reason: "More than one like found")}
			return try ApiLike(dbModel: like)
		}
	}
	
}
