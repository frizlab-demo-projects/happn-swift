import Foundation
import JWTKit
import Vapor



final class LoginController {
	
	func login(req: Request) throws -> ApiLoginInfo {
		let loggedInUser = try req.auth.require(User.self)
		guard let userID = loggedInUser.id else {
			throw Abort(.internalServerError, reason: "The user id should be present in an authenticated user")
		}
		
		let token = AuthToken(sub: userID, exp: Date() + TimeInterval(7 * 24 * 60 * 60))
		let signedToken = try JWTSigner.hs256(key: req.application.jwtSecret).sign(token)
		
		return ApiLoginInfo(userId: token.sub, token: signedToken, expirationDate: token.exp)
	}
	
}
