import Fluent
import Foundation
import Vapor



final class PicturesController {
	
	func get(req: Request) throws -> EventLoopFuture<[ApiPicture]> {
		guard let userID = req.parameters.get("userID") else {
			throw Abort(.internalServerError, reason: "userID is not set in the request")
		}
		return Picture.query(on: req.db).filter(\.$user.$id == userID).all().flatMapEachThrowing{ try ApiPicture(dbModel: $0) }
	}
	
}
