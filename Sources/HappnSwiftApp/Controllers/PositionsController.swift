import Foundation

import Vapor



final class PositionsController {
	
	func get(req: Request) throws -> EventLoopFuture<ApiPosition> {
		guard let userID = req.parameters.get("userID") else {
			throw Abort(.internalServerError, reason: "userID is not set in the request")
		}
		return User
			.find(userID, on: req.db)
			.unwrap(or: Abort(.notFound))
			.flatMapThrowing{ try ApiPosition(dbModel: $0) }
	}
	
}
