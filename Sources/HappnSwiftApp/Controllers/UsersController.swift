import Foundation

import Vapor



final class UsersController {
	
	func get(req: Request) throws -> EventLoopFuture<ApiUser> {
		let loggedInUser = try req.auth.require(User.self)
		guard let userID = req.parameters.get("userID") else {
			throw Abort(.internalServerError, reason: "userID is not set in the request")
		}
		return User
			.find(userID, on: req.db)
			.unwrap(or: Abort(.notFound))
			.flatMap{ $0.$pictures.load(on: req.db).transform(to: $0) }
			.flatMapThrowing{ try ApiUser(dbModel: $0, userIsMe: loggedInUser.id == userID) }
	}
	
}
