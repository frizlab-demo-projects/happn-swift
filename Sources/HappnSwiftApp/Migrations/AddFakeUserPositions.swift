import Fluent
import FluentSQL
import Foundation
import NIO



struct AddFakeUserPositions : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		var pictureFileHandleGlobal: NIOFileHandle?
		return database.eventLoop.future()
			.flatMapThrowing{ _ -> NIOFileHandle in
				let downloadsFolderURL = try FileManager.default.url(for: .downloadsDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
				let pictureFolderURL = downloadsFolderURL.appendingPathComponent("datafiles").appendingPathComponent("position")
				let picureFileURL = pictureFolderURL.appendingPathComponent("database").appendingPathExtension("txt")
				let fh = try NIOFileHandle(path: picureFileURL.path, mode: .read)
				pictureFileHandleGlobal = fh
				return fh
				
		}.flatMap{ pictureFileHandle -> EventLoopFuture<ByteBuffer> in
			let threadPool = NIOThreadPool(numberOfThreads: 1)
			threadPool.start()
			/* The thread pool should be shutdown at some point… */
			let fileio = NonBlockingFileIO(threadPool: threadPool)
			return fileio.read(fileHandle: pictureFileHandle, fromOffset: 0, byteCount: Int(Int32.max), allocator: ByteBufferAllocator(), eventLoop: database.eventLoop)
			
		}.flatMapThrowing{ bytes in
			/* Note: I’m aware this algorithm is terrible in term of memory use
			 *       and CPU cycles. We could probably use a SimpleStream to
			 *       enhance this, or something else! */
			guard let string = bytes.getString(at: 0, length: bytes.readableBytes) else {
				throw FakePositionMigrationError(reason: "Cannot get string from read file")
			}
			let lines = string.split(separator: "\n")
			return lines.compactMap{ line in
				let fields = line.split(separator: ",").map(String.init)
				guard fields.count == 3 else {
					database.logger.log(level: .warning, "Skipping position because it is not valid (not exactly 3 fields): \(line)")
					return nil
				}
				guard let lat = Double(fields[1]) else {
					database.logger.log(level: .warning, "Skipping position because it is not valid (cannot parse latitude): \(line)")
					return nil
				}
				guard let long = Double(fields[2]) else {
					database.logger.log(level: .warning, "Skipping position because it is not valid (cannot parse longitude): \(line)")
					return nil
				}
				
				return User
				.find(fields[0], on: database)
				.flatMap{ userOrNil in
					guard let user = userOrNil else {
						database.logger.log(level: .warning, "Skipping position because it’s associated user cannot be found: \(line)")
						return database.eventLoop.makeSucceededFuture(())
					}
					user.position = ApiUser.Position(latitude: lat, longitude: long)
					return user.save(on: database)
				}
			}
		}.flatMap{ futures in
			return EventLoopFuture<Void>.andAllSucceed(futures, on: database.eventLoop)
			
		}.flatMapErrorThrowing{ error in
			database.logger.log(level: .error, "Cannot import fake positions. Don’t caring though. Error was: \(error)")
			
		}.always{ _ in
			/* Let’s say we don’t care about closure issue. It’s bad, but not
			  * too bad either; we were only reading from the file! */
			_ = try? pictureFileHandleGlobal?.close()
			pictureFileHandleGlobal = nil
		}
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.eventLoop.makeFailedFuture(FakePositionMigrationError(reason: "Not Implemented"))
	}
	
	private struct FakePositionMigrationError : Error {
		var reason: String
	}
	
}
