import Fluent
import FluentSQL
import Foundation
import NIO



struct CreateFakeUserUserLikes : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "en_US_POSIX")
		dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		
		var likesFileHandleGlobal: NIOFileHandle?
		return database.eventLoop.future()
			.flatMapThrowing{ _ -> NIOFileHandle in
				let downloadsFolderURL = try FileManager.default.url(for: .downloadsDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
				let likeFolderURL = downloadsFolderURL.appendingPathComponent("datafiles").appendingPathComponent("like")
				let likeFileURL = likeFolderURL.appendingPathComponent("database").appendingPathExtension("txt")
				let fh = try NIOFileHandle(path: likeFileURL.path, mode: .read)
				likesFileHandleGlobal = fh
				return fh
				
		}.flatMap{ likeFileHandle -> EventLoopFuture<ByteBuffer> in
			let threadPool = NIOThreadPool(numberOfThreads: 1)
			threadPool.start()
			/* The thread pool should be shutdown at some point… */
			let fileio = NonBlockingFileIO(threadPool: threadPool)
			return fileio.read(fileHandle: likeFileHandle, fromOffset: 0, byteCount: Int(Int32.max), allocator: ByteBufferAllocator(), eventLoop: database.eventLoop)
			
		}.flatMapThrowing{ bytes in
			/* Note: I’m aware this algorithm is terrible in term of memory use
			 *       and CPU cycles. We could probably use a SimpleStream to
			 *       enhance this, or something else! */
			guard let string = bytes.getString(at: 0, length: bytes.readableBytes) else {
				throw FakeUserUserLikesMigrationError(reason: "Cannot get string from read file")
			}
			let lines = string.split(separator: "\n")
			return lines.compactMap{ line in
				let fields = line.split(separator: ",").map(String.init)
				guard fields.count == 3 else {
					database.logger.log(level: .warning, "Skipping like because it is not valid (not exactly 3 fields): \(line)")
					return nil
				}
				guard let date = dateFormatter.date(from: fields[2]) else {
					database.logger.log(level: .warning, "Skipping like because it is not valid (cannot parse date): \(line)")
					return nil
				}
				
				let l = UserUserLike()
				l.$from.id = fields[0]
				l.$to.id = fields[1]
				l.creationDate = date /* Funny thing, because the creationDate is a
				                       * @Timestamp, the value is ignored on save. */
				/* We could/should probably batch. Don’t know how, nor do I care. */
				return l.save(on: database)
			}
		}.flatMap{ futures in
			return EventLoopFuture<Void>.andAllSucceed(futures, on: database.eventLoop)
			
		}.flatMapErrorThrowing{ error in
			database.logger.log(level: .error, "Cannot import fake likes. Don’t caring though. Error was: \(error)")
			
		}.always{ _ in
			/* Let’s say we don’t care about closure issue. It’s bad, but not
			  * too bad either; we were only reading from the file! */
			_ = try? likesFileHandleGlobal?.close()
			likesFileHandleGlobal = nil
		}
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.eventLoop.makeFailedFuture(FakeUserUserLikesMigrationError(reason: "Not Implemented"))
	}
	
	private struct FakeUserUserLikesMigrationError : Error {
		var reason: String
	}
	
}
