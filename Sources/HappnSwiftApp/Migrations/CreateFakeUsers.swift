import Fluent
import FluentSQL
import Foundation
import NIO



struct CreateFakeUsers : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		/* Let’s read the users from the generated file */
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "en_US_POSIX")
		dateFormatter.dateFormat = "yyyy-MM-dd"
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		
		var userFileHandleGlobal: NIOFileHandle?
		return database.eventLoop.future()
		.flatMapThrowing{ _ -> NIOFileHandle in
			let downloadsFolderURL = try FileManager.default.url(for: .downloadsDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
			let userFolderURL = downloadsFolderURL.appendingPathComponent("datafiles").appendingPathComponent("user")
			let userFileURL = userFolderURL.appendingPathComponent("database").appendingPathExtension("txt")
			let fh = try NIOFileHandle(path: userFileURL.path, mode: .read)
			userFileHandleGlobal = fh
			return fh
			
		}.flatMap{ userFileHandle -> EventLoopFuture<ByteBuffer> in
			let threadPool = NIOThreadPool(numberOfThreads: 1)
			threadPool.start()
			/* The thread pool should be shutdown at some point… */
			let fileio = NonBlockingFileIO(threadPool: threadPool)
			return fileio.read(fileHandle: userFileHandle, fromOffset: 0, byteCount: Int(Int32.max), allocator: ByteBufferAllocator(), eventLoop: database.eventLoop)
			
		}.flatMapThrowing{ bytes in
			/* Note: I’m aware this algorithm is terrible in term of memory use
			 *       and CPU cycles. We could probably use a SimpleStream to
			 *       enhance this, or something else! */
			guard let string = bytes.getString(at: 0, length: bytes.readableBytes) else {
				throw FakeUserMigrationError(reason: "Cannot get string from read file")
			}
			let lines = string.split(separator: "\n")
			return lines.compactMap{ line in
				let fields = line.split(separator: ",").map(String.init)
				guard fields.count >= 7 else {
					database.logger.log(level: .warning, "Skipping user because it is not valid (less than 7 fields): \(line)")
					return nil
				}
				guard let date = dateFormatter.date(from: fields[2]) else {
					database.logger.log(level: .warning, "Skipping user because it is not valid (cannot parse date): \(line)")
					return nil
				}
				guard let gender = ApiUser.Gender(rawValue: fields[3].lowercased()) else {
					database.logger.log(level: .warning, "Skipping user because it is not valid (cannot parse gender): \(line)")
					return nil
				}
				guard let minAge = Int(fields[4]) else {
					database.logger.log(level: .warning, "Skipping user because it is not valid (cannot parse min age): \(line)")
					return nil
				}
				guard let maxAge = Int(fields[5]) else {
					database.logger.log(level: .warning, "Skipping user because it is not valid (cannot parse max age): \(line)")
					return nil
				}
				var genders = Set<ApiUser.Gender>()
				for gStr in fields[6...] {
					guard let gender = ApiUser.Gender(rawValue: gStr.trimmingCharacters(in: CharacterSet.letters.inverted).lowercased()) else {
						database.logger.log(level: .warning, "Skipping user because it is not valid (cannot parse one of the preference gender): \(line)")
						return nil
					}
					genders.insert(gender)
				}
				
				let u = User()
				u.id = fields[0]
				u.name = fields[1]
				u.birthDate = date
				u.gender = gender
				u.preferences = ApiUser.Preferences(ageMin: minAge, ageMax: maxAge, genders: genders)
				/* We could/should probably batch. Don’t know how, nor do I care. */
				return u.save(on: database)
			}
		}.flatMap{ futures in
			return EventLoopFuture<Void>.andAllSucceed(futures, on: database.eventLoop)
			
		}.flatMapErrorThrowing{ error in
			database.logger.log(level: .error, "Cannot import fake users. Don’t caring though. Error was: \(error)")
			
		}.always{ _ in
			/* Let’s say we don’t care about closure issue. It’s bad, but not
			  * too bad either; we were only reading from the file! */
			_ = try? userFileHandleGlobal?.close()
			userFileHandleGlobal = nil
		}
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.eventLoop.makeFailedFuture(FakeUserMigrationError(reason: "Not Implemented"))
	}
	
	private struct FakeUserMigrationError : Error {
		var reason: String
	}
	
}
