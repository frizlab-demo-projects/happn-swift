import Fluent
import FluentSQL



struct CreatePicturesTable : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(Picture.schema)
			.id()
			.field("size", .string, .required)
			.field("url", .string, .required)
			.field("user_id", .string, .references(User.schema, "id"))
			.unique(on: "id")
			.create()
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(Picture.schema).delete()
	}
	
}
