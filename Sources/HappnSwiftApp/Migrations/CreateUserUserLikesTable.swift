import Fluent
import FluentSQL



struct CreateUserUserLikesTable : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(UserUserLike.schema)
			.id()
			.field("created_at", .datetime)
			.field("from_id", .string, .references(User.schema, "id"))
			.field("to_id", .string, .references(User.schema, "id"))
			.unique(on: "id")
			.create()
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(UserUserLike.schema).delete()
	}
	
}
