import Fluent
import FluentSQL



struct CreateUsersTable : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(User.schema)
			.field("id", .string, .identifier(auto: false), .required)
			.field("name", .string, .required)
			/* We use the `.float` type here instead of `.date`.
			 * See https://github.com/vapor/sqlite-nio/pull/16 for a rationale.
			 * When (if) the PR is merged, we will be able to use `.date`. */
			.field("birth_date", .float, .required)
			.field("gender", .string, .required)
			.field("preferences", .json, .required)
			.unique(on: "id")
			.create()
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(User.schema).delete()
	}
	
}
