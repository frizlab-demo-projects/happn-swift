import Fluent
import FluentSQL



struct UpdateUsersTableForPosition : Migration {
	
	func prepare(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(User.schema)
			.field("position", .json)
			.update()
		/* SQLite cannot batch modify a table. If needed, we’d do this:
		return database.eventLoop.future()
			.flatMap{ database.schema(User.schema).field("latitude", .double).update() }
			.flatMap{ database.schema(User.schema).field("longitude", .double).update() } */
	}
	
	func revert(on database: Database) -> EventLoopFuture<Void> {
		return database.schema(User.schema)
			.deleteField("position")
			.update()
		/* SQLite cannot batch modify a table. If needed, we’d do this:
		return database.eventLoop.future()
			.flatMap{ database.schema(User.schema).deleteField("latitude").update() }
			.flatMap{ database.schema(User.schema).deleteField("longitude").update() } */
	}
	
}
