import Foundation
import Vapor



public struct ApiLike : Codable, Content {
	
	public var userIdFrom: String
	public var userIdTo: String
	public var creationDate: Date
	
	public init(userIdFrom: String, userIdTo: String, creationDate: Date) {
		self.userIdFrom = userIdFrom
		self.userIdTo = userIdTo
		self.creationDate = creationDate
	}
	
	init(dbModel like: UserUserLike) throws {
		guard let date = like.creationDate else {
			throw Abort(.internalServerError, reason: "A saved like should have a creation date")
		}
		self.userIdFrom = like.$from.id
		self.userIdTo = like.$to.id
		self.creationDate = date
	}
	
}
