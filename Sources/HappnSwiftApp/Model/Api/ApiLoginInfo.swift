import Foundation
import Vapor



struct ApiLoginInfo : Content {
	
	var userId: String
	
	var token: String
	var expirationDate: Date
	
}
