import Foundation
import Vapor



struct ApiLoginPostData : Content {
	
	var userId: String
	var iHaveThePassword: Bool
	
}
