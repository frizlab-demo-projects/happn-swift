import Foundation
import Vapor



public struct ApiPicture : Codable, Content {
	
	public enum Size : String, Codable {
		
		case large
		case medium
		case thumbnail = "thumb"
		
	}
	
	public var pictureId: String
	public var userId: String?
	public var pictureFormat: Size
	public var pictureURL: URL
	
	public init(pictureId: String, userId: String, pictureFormat: Size, pictureURL: URL) {
		self.pictureId = pictureId
		self.userId = userId
		self.pictureFormat = pictureFormat
		self.pictureURL = pictureURL
	}
	
	init(dbModel picture: Picture) throws {
		guard let id = picture.id else {
			throw Abort(.internalServerError, reason: "Cannot create ApiPicture from a db Picture")
		}
		self.pictureId = id.uuidString
		self.userId = picture.$user.id
		self.pictureFormat = picture.size
		self.pictureURL = picture.url
	}
	
}
