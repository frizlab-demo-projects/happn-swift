import Foundation
import Vapor



public struct ApiPosition : Codable, Content {
	
	public enum Size : String, Codable {
		
		case large
		case medium
		case thumbnail = "thumb"
		
	}
	
	public var userId: String?
	public var latitude: Double
	public var longitude: Double
	
	public init(userId: String, latitude: Double, longitude: Double) {
		self.userId = userId
		self.latitude = latitude
		self.longitude = longitude
	}
	
	init(dbModel user: User) throws {
		guard let id = user.id else {
			throw Abort(.internalServerError, reason: "Cannot create ApiPosition from a db User")
		}
		guard let position = user.position else {
			/* We should not really throw a not found error at this level but 🤷‍♂️ */
			throw Abort(.notFound)
		}
		self.userId = id
		self.latitude = position.latitude
		self.longitude = position.longitude
	}
	
}
