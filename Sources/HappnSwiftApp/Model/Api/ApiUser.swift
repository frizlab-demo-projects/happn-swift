import Foundation
import Vapor



public struct ApiUser : Codable, Content {
	
	public enum Gender : String, Codable {
		
		case male
		case female
		
	}
	
	public struct Preferences : Codable {
		
		public var ageMin: Int
		public var ageMax: Int
		public var genders = Set<Gender>()
		
	}
	
	public struct Position : Codable {
		
		public var latitude: Double
		public var longitude: Double
		
	}
	
	public var userId: String
	public var name: String
	public var birthDate: Date
	public var gender: Gender
	public var preferences: Preferences?
	
	public var pictures: [ApiPicture]?
	
	public var position: Position?
	
	public init(userId: String, name: String, birthDate: Date, gender: Gender, preferences: Preferences, position: Position?) {
		self.userId = userId
		self.name = name
		self.birthDate = birthDate
		self.gender = gender
		self.preferences = preferences
		self.position = position
	}
	
	init(dbModel user: User, userIsMe: Bool) throws {
		guard let id = user.id else {
			throw Abort(.internalServerError, reason: "Cannot create ApiUser from a db User")
		}
		self.userId = id
		self.name = user.name
		self.birthDate = user.birthDate
		self.gender = user.gender
		self.preferences = userIsMe ? user.preferences : nil
		
		self.pictures = try user.$pictures.value?.map{ try ApiPicture(dbModel: $0) }
		
		self.position = user.position
	}
	
}
