import Foundation
import Fluent
import Vapor



final class Picture : Model, Content {
	
	static let schema = "pictures"
	
	@ID(key: .id)
	var id: UUID?
	
	@Field(key: "size")
	var size: ApiPicture.Size
	
	@Field(key: "url")
	var url: URL
	
	@Parent(key: "user_id")
	var user: User
	
}
