import Foundation
import Fluent
import Vapor



final class User : Model, Authenticatable {
	
	static let schema = "users"
	
	@ID(custom: "id")
	var id: String?
	
	@Field(key: "name")
	var name: String
	
	@Field(key: "birth_date")
	var birthDate: Date
	
	@Field(key: "gender")
	var gender: ApiUser.Gender
	
	@Field(key: "preferences")
	var preferences: ApiUser.Preferences
	
	@Field(key: "position")
	var position: ApiUser.Position?
	
	@Children(for: \Picture.$user)
	var pictures: [Picture]
	
	@Siblings(through: UserUserLike.self, from: \.$from, to: \.$to)
	var likes: [User]
	
	@Siblings(through: UserUserLike.self, from: \.$to, to: \.$from)
	var liked: [User]
	
}
