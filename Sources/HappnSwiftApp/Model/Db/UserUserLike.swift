import Foundation
import Fluent
import Vapor



final class UserUserLike : Model {
	
	static let schema = "user_user_likes"
	
	@ID(key: .id)
	var id: UUID?
	
	@Timestamp(key: "created_at", on: .create)
	var creationDate: Date?
	
	@Parent(key: "from_id")
	var from: User
	
	@Parent(key: "to_id")
	var to: User
	
}
