import Vapor
import Fluent
import FluentSQLiteDriver


public func configure(_ app: Application) throws {
	/* Configure default JSON encoder so it converts keys to snake case. */
	let encoder = JSONEncoder()
	encoder.dateEncodingStrategy = .iso8601
	encoder.keyEncodingStrategy = .convertToSnakeCase
	ContentConfiguration.global.use(encoder: encoder, for: .json)
	
	/* Configure default JSON decodre so it converts keys from snake case. */
	let decoder = JSONDecoder()
	decoder.dateDecodingStrategy = .iso8601
	decoder.keyDecodingStrategy = .convertFromSnakeCase
	ContentConfiguration.global.use(decoder: decoder, for: .json)
	
	app.jwtSecret = Data("This secret is so awesome, it’s for our eyes only…".utf8)
	
	app.databases.use(.sqlite(.file("db.sqlite")), as: .sqlite)
	
	app.migrations.add(CreateUsersTable())
	app.migrations.add(CreateFakeUsers())
	
	app.migrations.add(CreatePicturesTable())
	app.migrations.add(CreateFakePictures())
	
	app.migrations.add(CreateUserUserLikesTable())
	app.migrations.add(CreateFakeUserUserLikes())
	
	app.migrations.add(UpdateUsersTableForPosition())
	app.migrations.add(AddFakeUserPositions())
	
	try routes(app)
}
