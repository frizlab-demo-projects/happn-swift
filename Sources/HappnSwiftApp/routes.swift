import Vapor



func routes(_ app: Application) throws {
	let apiGroup = app.grouped("api")
	let apiAuthGroup = apiGroup.grouped("auth")
	let guardedApiGroup = apiGroup.grouped(UserBearerAuthenticator(), User.guardMiddleware())
	
	let loginController = LoginController()
	apiAuthGroup.grouped(UserTrustAuthenticator()).post("login", use: loginController.login)
	
	let usersController = UsersController()
	guardedApiGroup.get("users", ":userID", use: usersController.get)
	
	let picturesController = PicturesController()
	guardedApiGroup.get("pictures", "user", ":userID", use: picturesController.get)
	
	let likesController = LikesController()
	guardedApiGroup.get("likes", "from_user", ":userIDFrom", "to_user", ":userIDTo", use: likesController.get)
	
	let positionsController = PositionsController()
	guardedApiGroup.get("positions", "user", ":userID", use: positionsController.get)
}
