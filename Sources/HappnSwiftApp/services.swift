import Vapor


extension Application {
	
	var jwtSecret: Data {
		get {storage[JWTSecretConfigKey.self]!}
		set {storage[JWTSecretConfigKey.self] = newValue}
	}
	
	
	private struct JWTSecretConfigKey: StorageKey {
		typealias Value = Data
	}
	
}
